import React, { lazy, Suspense } from 'react';
import { Navbar } from './core/components/Navbar';
import { BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import { PageHome } from './features/PageHome';
import { PrivateRoute } from './core/auth/PrivateRoute';

const UsersPage = lazy(() => import('./features/users/UsersPage'));
const PageSettings = lazy(() => import('./features/PageSettings'));

function App() {
  return (
    <div className="centered">
      <BrowserRouter>
        <Navbar />
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <Route path='/' exact={true}>
              <PageSettings />
            </Route>
            <Route path='/home'>
              <PageHome />
            </Route>
            <PrivateRoute path='/users'>
              <UsersPage />
            </PrivateRoute>
            <PrivateRoute path='/settings'>
              <PageSettings />
            </PrivateRoute>
          </Switch>
        </Suspense>
      </BrowserRouter>

    </div>
  )
}

export default App;
