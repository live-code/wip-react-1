import { useEffect, useState } from 'react';
import { Friend, FriendNoID } from '../model/friend';
import Axios from 'axios';

export function useFriend() {
  const [friends, setFriends] = useState<Friend[]>([])
  const [url] = useState('tweets');

  useEffect(() => {
    Axios.get<Friend[]>('http://localhost:3001/tweets')
      .then(result => {
        setFriends(result.data);
      })
  }, [] )

  function addFriend(value: FriendNoID) {
    Axios.post<Friend>(`http://localhost:3001/tweets`, value)
      .then(res => setFriends([...friends, res.data]))
  }

  function deleteFriend(user: Friend, endpoint: string) {
    Axios.delete(`http://localhost:3001/${endpoint}/${user.id}`)
      .then(() => {
        setFriends(
          friends.filter(f => f.id !== user.id)
        );
      })
  }

  return {
    friends,
    url,
    deleteFriend,
    addFriend
  }
}
