import React from 'react';
import { Friend } from './model/friend';
import { useFriend } from './hooks/useFriend';
import { UserForm } from './components/UserForm';
import { UserList } from './components/UserList';
import { UserTotal } from './components/UserTotal';

function UsersPage() {
  const { friends, url, addFriend, deleteFriend, } = useFriend();

  return (
    <div className="centered">
      <UserForm
        onAddFriend={addFriend}
      />
      <UserList
        data={friends}
        onDeleteFriend={(f: Friend) => deleteFriend(f, url)}
      />
      <UserTotal friends={friends} />
    </div>
  )
}

export default UsersPage;
