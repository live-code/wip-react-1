import React, { useState } from 'react';
import { FriendNoID } from '../model/friend';
import cn from 'classnames';

interface UserFormProps {
  onAddFriend: (friend: FriendNoID) => void;
}
export const UserForm: React.FC<UserFormProps> = props => {
  const [formData, setFormData] = useState<FriendNoID>({ name: '', tweets: 0 })
  const [dirty, setDirty] = useState<boolean>(false);

  const nameIsValid = formData.name.length > 2;
  const tweetIsValid = formData.tweets > 2;
  const valid = nameIsValid && tweetIsValid;

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    const type = e.currentTarget.type;
    setFormData({
      ...formData,
      [e.currentTarget.name]: type === 'number' ? +e.currentTarget.value : e.currentTarget.value,
    });
    setDirty(true)
  }

  function saveHandler(e: React.FormEvent<HTMLFormElement>) {
    console.log(dirty, valid, nameIsValid)
    e.preventDefault()
    props.onAddFriend(formData);
  }

  return <div>
    { dirty && <div>Form non è valido</div>}
    <form onSubmit={saveHandler}>
      <input
        type="text"
        name="name"
        className={cn(
         'form-control',
         { 'is-invalid': !nameIsValid && dirty}
        )} value={formData.name} onChange={onChangeHandler} placeholder="name"/>

      <input
        type="number"
        name="tweets"
        className={cn(
          'form-control',
          { 'is-invalid': !tweetIsValid && dirty }
        )}
        value={formData.tweets} onChange={onChangeHandler} placeholder="tweets"/>
      <button type="submit" disabled={!valid} >Save</button>
    </form>
  </div>
}
