import React, { useState } from 'react';
import { Friend } from '../model/friend';
import cn from 'classnames';

interface UserListProps {
  friend: Friend;
  onDeleteFriend: (friend: Friend) => void
}

export const UserListItem: React.FC<UserListProps> = ({ friend, onDeleteFriend }) => {
  const [open, setOpened] = useState<boolean>(false)

  return <li className="list-group-item">
    {friend.name} - {friend.tweets}
    <i
      className="fa fa-trash"
      onClick={() => onDeleteFriend(friend)}
    />

    <i
      className={cn(
        'fa',
        { 'fa-arrow-circle-down': open, 'fa-arrow-circle-up': !open }
      )}
      onClick={() => setOpened(!open)} />

    { open && <div>Contenuto extra</div>}
  </li>
};
