import React  from 'react';
import { Friend } from '../model/friend';

export const UserTotal: React.FC<{ friends: Friend[]}> = (props) => {

  const getTotal = () => props.friends.reduce((acc: number, item: Friend) => {
    return acc + item.tweets
  }, 0);

  return <div>
    TOTAL: {getTotal()}
  </div>
};
