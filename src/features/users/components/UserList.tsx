import React from 'react';
import { Friend } from '../model/friend';
import { UserListItem } from './UserListItem';

interface UserListProps {
  data: Friend[];
  onDeleteFriend: (friend: Friend) => void
}

export const UserList: React.FC<UserListProps> = (props) => {
  return <div>
    { props.data?.length } risultati

    {
      props.data.map((friend, index) => {
        return <UserListItem key={friend.id}  friend={friend} onDeleteFriend={props.onDeleteFriend} />
      })
    }
  </div>
}
