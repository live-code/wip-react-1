// model/friend.ts
export interface Friend {
  id: number;
  name: string;
  tweets: number;
}

export type FriendNoID = Omit<Friend, 'id'>;
