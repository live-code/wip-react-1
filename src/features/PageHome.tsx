import React, { useEffect } from 'react';
import Axios from 'axios';
import { useHistory } from 'react-router-dom';
import { Auth, Credentials, signIn } from '../core/auth/authentication.service';

export const PageHome: React.FC = () => {
  const history = useHistory();


  const login = () => {
    const credentials: Credentials = {
      username: 'abc',
      password: '123'
    };

    signIn('http://localhost:3001/login', credentials)
      .then((res: Auth) => {
        console.log(res)
        history.replace('users');
      })
  };

  return <div>
    <h1>Login</h1>
    <input type="text" placeholder="user"/>
    <input type="password" placeholder="pass" />
    <button onClick={login}>Sign In</button>
    <hr/>
    <p>You must log in to view the private pages (Settings & Admin)</p>
  </div>
};
