import { getItemFromLocalStorage, removeItemLocalStorage, setItemLocalStorage } from './localstorage.helper';
import Axios from 'axios';

export interface Credentials {
  username: string;
  password: string;
}

export interface Auth {
  accessToken: string;
}
export function signIn(url: string, params: Credentials): Promise<Auth> {

  const api = `${url}?username=${params.username}&password=${params.password}`;

  return Axios.get<Auth>(api)
    .then(res => {
      setItemLocalStorage('token', res.data.accessToken);
      return res.data
    })

}

export function signOut() {
  removeItemLocalStorage('token');
}

export function isLogged(): boolean {
  return !!getItemFromLocalStorage('token')
}
