import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router';
import { isLogged } from './authentication.service';

interface PrivateRouteProps {
  path: string;
}

export const PrivateRoute: React.FC<PrivateRouteProps & RouteProps> = ({ children, ...rest }) => {
  return  <Route {...rest}>
    {
      isLogged()  ?
        children :
        <Redirect to={{ pathname: '/home'}}/>
    }
  </Route>
}
