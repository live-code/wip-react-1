import React  from 'react';
import { Link, NavLink, useHistory, useLocation } from 'react-router-dom';
import css from './Navbar.module.css';
import { isLogged, signOut } from '../auth/authentication.service';

export const Navbar: React.FC = () => {
  const history = useHistory();
  const {  pathname } = useLocation();

  const signOutHandler = () => {
    history.push("/home");
    signOut();
  };

  return isLogged() ? <div>
    <div className="btn-group">
      <NavLink className="btn btn-primary" to="/home" activeClassName={css.bg}>
        <div>Home</div>
      </NavLink>
      <NavLink className="btn btn-primary" to="/settings" activeClassName={css.bg}>
        <div>settings</div>
      </NavLink>
      <NavLink className="btn btn-primary" to="/users" activeClassName={css.bg}>
        <div>users</div>
      </NavLink>

      <button className="btn btn-primary" onClick={signOutHandler}>Logout</button>
    </div>
    <div>Path:     {pathname}</div>
  </div> : null
};
